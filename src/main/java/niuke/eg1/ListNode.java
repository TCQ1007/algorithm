package niuke.eg1;

/**
 * @author will.tuo
 * @date 2021/11/12 10:57
 */
public class ListNode {

    int val;
    ListNode next = null;

    @Override
    public String toString() {
        return "ListNode{" +
                "val=" + val +
                ", next=" + next +
                '}';
    }

    ListNode(int val) {
        this.val = val;
    }

    public ListNode() {

    }
}
