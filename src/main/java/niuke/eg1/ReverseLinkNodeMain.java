package niuke.eg1;

/**
 * @author will.tuo
 * @date 2021/11/12 10:56
 * <p>
 * 反转链表
 */
public class ReverseLinkNodeMain {

    public static void main(String[] args) {

        ListNode head = new ReverseLinkNodeMain().buildLinkNode();
        System.out.println(head);
        ListNode head1 = ListNodeBuilder.reverseList(head);
        System.out.println(head1);
    }

    public ListNode buildLinkNode() {
        ListNodeBuilder listNodeBuilder = new ListNodeBuilder();
        listNodeBuilder.build(new ListNode(1));
        listNodeBuilder.addNode(new ListNode(2));
        listNodeBuilder.addNode(new ListNode(3));
        return listNodeBuilder.headNode;
    }
}
