package niuke.eg1;

/**
 * @author will.tuo
 * @date 2021/11/12 11:00
 */
public class ListNodeBuilder {

    public ListNode tailNode = new ListNode();
    public ListNode headNode = new ListNode();

    public ListNode build() {
        return build(new ListNode());
    }

    public ListNode build(ListNode node) {
        headNode = node;
        tailNode = headNode;
        return headNode;
    }

    public ListNode addNode(ListNode node) {
        tailNode.next = node;
        tailNode = node;
        return tailNode;
    }

    public static ListNode reverseList(ListNode head) {
        if (head == null) {
            return null;
        }
        ListNode nextNode = head.next;
        head.next = null;
        while (nextNode != null) {
            ListNode tmpNode = nextNode.next;
            nextNode.next = head;
            head = nextNode;
            nextNode = tmpNode;
        }
        return head;
    }
}
