package niuke.eg2;

import java.util.LinkedHashMap;
import java.util.Map;
import java.util.Map.Entry;

/**
 * @author will.tuo
 * @date 2021/11/12 14:40
 */
public class LRUCache<K, V> extends LinkedHashMap<K, V> {

    private static final int MAX_ENTRIES = 100;

    private static final float DEFAULT_LOAD_FACTOR = 0.75f;


    private final int maxCapacity;

    public LRUCache(int initialCapacity) {
        // 第三个参数为accessOrder
        super(initialCapacity, DEFAULT_LOAD_FACTOR, true);
        this.maxCapacity = initialCapacity;
    }

    @Override
    protected boolean removeEldestEntry(Map.Entry eldest) {
        return size() > this.maxCapacity;
    }

    public void print() {
        for (Entry entry : this.entrySet()) {
            System.out.print(entry.getKey() + ":" + entry.getValue() + "->");
        }
        System.out.println();
    }

}
