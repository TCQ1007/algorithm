package niuke.eg2;

import java.util.LinkedHashMap;

/**
 * @author will.tuo
 * @date 2021/11/12 14:19
 */
public class LinkedHashMapTest {

    public static void main(String[] args) {
        LinkedHashMap<Integer, Integer> linkedHashMap = new LinkedHashMap<Integer, Integer>(3, 0.75f, true);
        linkedHashMap.put(1, 1);
        linkedHashMap.put(2, 2);
        linkedHashMap.put(3, 3);
    }
}
